let FAClient = null;
let notyf = null;

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cDovL2xvY2FsaG9zdDo1MDAw`,
};

function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    FAClient.on('makeCall', ({phone="123"}) => {
        console.log({phone});
        call(phone);
    });

    const {
        clientId="MnsjFsJhS-GBUe_kNqLyCQ"
    } = FAClient.params;
    console.log(clientId);

    const iFrame = document.createElement('iframe');
    iFrame.src = `https://example.io/ringcentral-embeddable/app.html?appKey=${clientId}`;
    iFrame.style = 'width: 100%; height: 100%; border: none;';
    iFrame.allow = 'microphone';
    removeTextMessage();
    document.getElementById('frameContainer').appendChild(iFrame);
}

function call(phoneNumber) {
    console.log(phoneNumber)
}

function removeTextMessage() {
    const loadingText = document.getElementById('loading-text');
    loadingText.remove();
}

/*
function logMessages(messages, phoneNumbers, callback) {
    const messagesIDs = messages.map((m) => `${m.id}`);

    FAClient.listEntityValues(
        {
            entity: PHONE_APPLET_CONFIGURATION.name,
            filters: [
                {
                    field_name: PHONE_APPLET_CONFIGURATION.fields.ringcentralId,
                    operator: 'includes',
                    values: messagesIDs,
                },
            ],
        },
        (existingMessages) => {
            console.log(existingMessages);
            const pattern = parseNumbersForPattern(phoneNumbers);

            FAClient.listEntityValues(
                {
                    entity: currentApp,
                    pattern,
                    limit: 1,
                },
                (contacts) => {
                    const contact = _.get(contacts, '[0]');
                    console.log(contacts);
                },
            );
        }
    );
}
 */

